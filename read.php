<?php
$reader = new XMLReader;
$reader->open('events.xml');

$doc = new DOMDocument;
$activities = array();
$references = array();

// move to the first <product /> node
while ($reader->read() && $reader->name !== 'event');

// now that we're at the right depth, hop to the next <product/> until the end of the tree
while ($reader->name === 'event') {
    // now you can use $node without going insane about parsing
    $node = simplexml_import_dom($doc->importNode($reader->expand(), true));
    foreach( $node->children() as $details ) {
        if( $details->getName() == 'action' ) {
            // Add each activity type to the array
            $type = (string)$details['type'];
            $activities[] = $type;
        } elseif( $details->getName() == 'references' ) {
            foreach( $details->children() as $reference ) {
                // Add each activity type to the array
                $type = (string)$reference['type'];
                $references[] = $type;
            }
        } else { 
            echo "WARNING! Unknown tag encountered<br/>";
        }
    }


    // go to next <event ... />
    $reader->next('event');
}

var_dump(array_unique($activities));
echo "<br />";
var_dump(array_unique($references));
?>
