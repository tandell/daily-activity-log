<?php
$reader = new XMLReader;
$reader->open('events.xml');

$doc = new DOMDocument;

// The activity types located, is set.
$activityList = array();

// The reference types located, is set.
$referenceList = array();

// The array of elements discovered
$items = array();

// move to the first <product /> node
while ($reader->read() && $reader->name !== 'event');

// now that we're at the right depth, hop to the next <product/> until the end of the tree
while ($reader->name === 'event') {
    // now you can use $node without going insane about parsing
    $node = simplexml_import_dom($doc->importNode($reader->expand(), true));

    $item = array();

    // Get the date & store.
    $item["date"] = (string)$node['date'];

    foreach( $node->children() as $details ) {
        if( $details->getName() == 'action' ) {
            $type = (string)$details['type'];

            // Add each activity type to the arrays
            $item["type"] = $type;
            $activityList[] = $type;

            $item["description"] = (string)$details;
        } elseif( $details->getName() == 'references' ) {
            $referernces = array();
            foreach( $details->children() as $reference ) {
                // Add each activity type to the array
                $type = (string)$reference['type'];
                $referenceList[] = $type;

                $referenceItem = array();
                $referenceItem['url'] = (string)$reference['src'];
                $referenceItem['type'] = $type;
                $references[] = $referenceItem;
            }
            $item['references'] = $references;

            // Need to clear the variable for reuse...
            unset($references);
        } else {
            echo "WARNING! Unknown tag encountered<br/>";
        }
    }

    $items[] = $item;

    // go to next <event ... />
    $reader->next('event');
}

$activityList = array_unique($activityList);
$referenceList = array_unique($referenceList);
?>

<html>
  <head>
    <title>Daily Activity Log</title>
    <meta http-equiv="cache-control" content="no-cache, must-revalidate, post-check=0, pre-check=0" />
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

    <link href="http://fonts.googleapis.com/css?family=Fauna+One" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="events.css" />
    <script src="events.js"></script>
  </head>
  <body>
    <div class="header fixed at-top">What's happening ...</div>
      <!-- TODO Move to dedicated page and remove to make this page read only -->
      <form onsubmit="saveData()" method="post">
        <div class="container update-block">
          <div class="container flex-column shrink">
            <label><small>Activity: </small></label>
            <input list="actions" name="activity" />
          </div>
          <datalist id="actions">
            <?php
                // Generate list of actions based on the recorded ones
                foreach( $activityList as $activity ) {
                    echo "<option value=\"$activity\"></option>\n";
                }
            ?>
          </datalist>
          <div class="container flex-column grow">
            <label><small>Description: </small></label>
            <input type="text" name="description" id="description" />
          </div>
          <div class="container flex-column shrink">
            <input  type="button" onclick="addRef()" value="Add Ref" />
          </div>
          <div class="container flex-column shrink">
            <input  type="submit" value="Update" />
          </div>
        </div>
        <div class="container reference-block" id="references">
          <datalist id="types">
            <!-- Generate list of references based on the recorded ones -->
            <?php
                foreach( $referenceList as $reference ) {
                    echo "<option value=\"$reference\"></option>\n";
                }
            ?>
          </datalist>
        </div>
      </form>
      <!-- Display the events.xml entries here -->
      <?php
        foreach($items as $item) {
          echo "<div class=\"event-container\">\n";
          // Header and references
            echo "<div class=\"event-item\">\n";
              echo "<div class=\"event-title\">" . $item['description'] . "</div>\n";
              // for each metadata
              echo "<div class=\"event-metadata\">\n";
                echo "<div class=\"event-date\">" . $item['date'] . "</div>\n";
                if( array_key_exists('references', $item) ){ 
                  echo "<div class=\"event-references\">\n";
                    $comma = "";
                    echo "(";
                    foreach($item['references'] as $reference ) {
                      echo $comma;
                      echo "<a target=\"_blank\" href=\"" . $reference['url'] . "\">" . $reference['type'] . "</a>";
                      $comma = ", ";
                    }
                    echo ")\n";
                  echo "</div>\n";
                }
              echo "</div>\n";
            echo "</div>";
            // event type
            echo "<div class=\"event-item\">\n";
              echo "<span class=\"event-type\">" . $item['type'] . "</span>\n";
            echo "</div>";
          echo "</div>";
          // Done
        }
      ?>
  </body>
</html>
