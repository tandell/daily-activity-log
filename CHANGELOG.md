# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Notes and Conventions
- This project was massively forked from [joebew42](https://github.com/joebew42/daily-activity-log) in March 2016.
- This changelog adheres to the conventions laid out in [Keep a CHANGELOG](http://keepachangelog.com/).

## [Unreleased]

### Added

* Converted to use PHP to update the log.
* Used CSS3 for styling

### Changed
### Deprecated
### Removed
### Fixed
### Security
