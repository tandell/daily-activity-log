<?php

	$date = date('Y-m-d', time());
	$xmlfile = "events.xml";
	$xml = simplexml_load_file($xmlfile);
	$event = $xml->addChild('event');
	$event->addAttribute('date', $date);
	$event->addAttribute('id', bin2hex(random_bytes(8)));

	$action = $event->addChild('action', $_POST['description']);
	$action->addAttribute('type', $_POST['activity']);

	//
	$count = 1;
  $type = "type" . $count;
  $reference = "reference" . $count;
  
  $references;
  if( array_key_exists( $type, $_POST) ) {
      $references = $event->addChild('references');
  }
  
  while( array_key_exists( $type, $_POST ) )  {
      $item = $references->addChild('reference');
      $item->addAttribute('src', $_POST[$reference]);
      $item->addAttribute('type', $_POST[$type]);

      $count++;
      $type = "type" . $count;
      $reference = "reference" . $count;
  } 

  // In order to save the XML file neatly... need to do this fun stuff.
  $dom = new DOMDocument("1.0");
  $dom->preserveWhiteSpace = false;
  $dom->formatOutput = true;
  $dom->loadXML($xml->asXML());
  file_put_contents($xmlfile, $dom->saveXML());
?>
