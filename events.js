var count = 1;
function addRef()
{
	var referenceName = "reference" + count;
	var typeName = "type" + count;
	var refDivName = "refdiv" + count;

	var refs = document.getElementById('references');
	var refNode = document.createElement("div");
	refNode.setAttribute('id', refDivName);
	refNode.setAttribute('class', "container flex-row grow");
	refNode.setAttribute('style', "align-self: center; width: 90%");

	var contents = '<div class="container flex-column shrink">' + 
						'<label><small>Type:</small></label><input list="types" name="' + typeName + '"/>' + 
					'</div>' + 
					'<div class="container flex-column grow">' + 
						'<label><small>URL: </small></label><input type="text" name="' + referenceName + '"/>' + 
					'</div>' + 
					'<div class="container flex-column shrink">' + 
						'<input  type="button" onclick="removeRef(\'' + refDivName + '\')" value="Remove" />' + 
					'</div>';

	refNode.innerHTML = contents;
	refs.appendChild(refNode);

	count += 1;
}

function removeRef(elementId)
{
	var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
}

function saveData() {
	let formData = new FormData(document.querySelector('form'));
	fetch("events.php",
    {
        body: formData,
        method: 'POST'
	});
	window.location.reload();
}
