# Daily Activity Log Technical Design

The data source for the DAL needs to be adjustable to account for different use case scenerios. All data processing _should_ happen on the backend before sending to the client.

## TODO

* Auth
* Date parsing
* Deploy script w/ git integration

## Supported Data Sources

### XML Data File

### JSON Data File

### SQL DB

### Cloud Service